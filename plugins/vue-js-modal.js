// plugins/vue-js-modal.js
import Vue from 'vue'
import Modal from 'vue-js-modal/dist/ssr.nocss'

import 'vue-js-modal/dist/styles.css'

Vue.use(Modal)

/*
export default function(_, inject) {
  inject('modal', VModal)
}
*/
