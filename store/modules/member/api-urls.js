const BASE_URL = '/api/member/v1/store-member'
export default {
    DO_LIST: `${BASE_URL}/all`, //get      //사용 안함
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post       // 사용안함
//------------------------------------------------------------------------------------------------
    DO_LIST_PAGING: `${BASE_URL}/list?page={pageNum}`, //get            // 가맹정 정보 리스트
    DO_DETAIL: `${BASE_URL}/detail/id/{id}`, //get                      // 가맹점 정보 리스트 상세보기
    DO_CREATE: `${BASE_URL}/data`, //post                               // 가맹점 정보 등록
    DO_UPDATE: `${BASE_URL}/store-member-id/{id}`, //put                // 가맹점 정보 수정
    DO_DELETE: `${BASE_URL}/status/store-member-id/{id}`, //put (del)   // 가맹점 정보 삭제

    DO_CHECK_BUSINESS_NUMBER: `${BASE_URL}/check/business-number?businessNumber={businessNumber}`, //get // 사업자 등록번호 중복확인
    DO_CHECK_USERNAME: `${BASE_URL}/check/user-name?username={username}`, //get     //id 중복 확인



}
