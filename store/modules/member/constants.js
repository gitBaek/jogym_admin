export default {
    DO_LIST_PAGING: 'member/doListPaging',
    DO_DETAIL: 'member/doDetail',
    DO_DELETE: 'member/doDelete',
    DO_CREATE: 'member/doCreate',
    DO_CHECK_USERNAME: 'member/doCheckUsername',
    DO_CHECK_BUSINESS_NUMBER:'member/doCheckBusinessNumber',
    DO_UPDATE: 'member/doUpdate',
    DO_CREATE_COMMENT: 'member/doCreateComment',
    DO_LIST: 'member/doList'

}
