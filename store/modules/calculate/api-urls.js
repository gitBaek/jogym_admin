const BASE_URL = '/api/calculate/v1/calculate'
export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_LIST_PAGING: `${BASE_URL}/list/store-member/{storeMemberId}?page={pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/detail/history-id/{historyId}`, //get
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_UPDATE: `${BASE_URL}/calculate-history/{calculateHistoryId}`, //put
    DO_DELETE: `${BASE_URL}/status/store-member-id/{id}`, //del
    DO_CREATE: `${BASE_URL}/data/store-member/{storeMemberId}`, //post
}

