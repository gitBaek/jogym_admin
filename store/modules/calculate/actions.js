import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_PAGING.replace('{storeMemberId}', payload.storeMemberId).replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        console.log(payload)
        return axios.get(apiUrls.DO_DETAIL.replace('{historyId}', payload.id))
    },

    [Constants.DO_DELETE]: (store, payload) => {
        return axios.put(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE.replace('{storeMemberId}', payload.id), payload.data)
    },
    [Constants.DO_CHECK_USERNAME]: (store, payload) => {
        return axios.get(apiUrls.DO_CHECK_USERNAME.replace('{username}', payload.username))
    },

    /*----------------------------------------------------------------------------------------------------------------*/
    [Constants.DO_CREATE_COMMENT]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE_COMMENT.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{calculateHistoryId}', payload.id), payload.data)
    },
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
}
