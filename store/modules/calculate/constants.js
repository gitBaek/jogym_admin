export default {
    DO_LIST_PAGING: 'calculate/doListPaging',
    DO_DETAIL: 'calculate/doDetail',
    DO_DELETE: 'calculate/doDelete',
    DO_CREATE: 'calculate/doCreate',
    DO_UPDATE: 'calculate/doUpdate',
    DO_LIST: 'calculate/doList'

}
