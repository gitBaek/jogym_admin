export default {
    DO_DELETE: 'fees-rate/doDelete',
    DO_CREATE: 'fees-rate/doCreate',
    DO_UPDATE: 'fees-rate/doUpdate',
    DO_LIST: 'fees-rate/doList'

}
