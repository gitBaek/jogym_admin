const BASE_URL = '/api/calculate/v1/fees-rate'
export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_UPDATE: `${BASE_URL}/fees-rate-id/{id}`, //put
    DO_DELETE: `${BASE_URL}/fees-rate-id/{id}`, //del
    DO_CREATE: `${BASE_URL}/data`, //post
}
