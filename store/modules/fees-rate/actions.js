import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {

    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },

    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },

    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.data)
    },

    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
}
