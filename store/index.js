import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import calculateHistory from "./modules/calculate";
import feesRate from './modules/fees-rate'
import member from './modules/member'
import menu from './modules/menu'
import testData from './modules/test-data'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    calculateHistory,
    feesRate,
    member,
    menu,
    testData
}
